export const SUCCESS = 200;
export const NOT_FOUND = 404;
export const INVALID_DATA = 422;
