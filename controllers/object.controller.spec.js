import { findOne, insert } from './db.actions';
import DB from '../db';
import { beforeAll } from '@jest/globals';
const delay = (time) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
beforeAll(async () => {
  await DB.drop();
  await DB.sync({ alter: true });
});
describe('****** Testing isolated functions ******', () => {
  const newObject = {
    key: String(Math.random()),
    value: String(Math.random()),
  };
  test('findOne should return null if it does not find a match', async () => {
    const result = await findOne(newObject.key);
    expect(result).toBe(null);
  });
  test('insert should return the inserted object', async () => {
    const result = await insert(newObject.key, newObject.value);
    const resJson = result || {};
    expect(resJson.key).toEqual(newObject.key);
    expect(resJson.value).toEqual(newObject.value);
  });
  test('Finds the latest record of the given key', async () => {
    await insert('test', 'testing');
    await delay(50);
    await insert('test', 'testing');
    await delay(50);
    const three = await insert('test', 'testing');
    const timestamp = three?.timestamp;
    const match = await findOne('test');
    expect(match.timestamp).toEqual(timestamp);
  });
  test('Finds the exact record if an existing timestamp is provided', async () => {
    const one = await insert('test', 'testing');
    await delay(50);
    await insert('test', 'testing');
    await delay(50);
    await insert('test', 'testing');
    const match = await findOne('test', one.timestamp);
    expect(match.timestamp).toEqual(one.timestamp);
  });
  test('Finds the most recent but less than the provided timestamp, if the given time is not an exact match', async () => {
    await insert('test', 'testing');
    await delay(50);
    const two = await insert('test', 'testing');
    await delay(50);
    const time = +new Date();
    await delay(50);
    await insert('test', 'testing');
    const match = await findOne('test', time);
    expect(match.timestamp).toEqual(two.timestamp);
  });
});
