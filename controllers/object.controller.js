import { findOne, insert } from './db.actions';
import { SUCCESS, NOT_FOUND, CREATED, INVALID_DATA } from '../statusCodes';
/**
 *
 * @description
 * Functions that deal with http requests
 */

/**
 * @description look for object, and send it if found, or send null otherwise
 * @param {object} req
 * @param {object} res
 * @returns {void}
 */
export const findObject = async (req, res) => {
  try {
    const { key } = req.params;
    let timestamp = req.query?.timestamp;
    const data = await findOne(key, timestamp);
    if (data) {
      res.status(SUCCESS).send(data);
    } else {
      res.status(NOT_FOUND).send(null);
    }
  } catch (error) {
    throw error;
  }
};
/**
 * @description Create or update the object, and send it back
 * @param {object} req
 * @param {object} res
 * @returns {void}
 */
export const insertObject = async (req, res) => {
  try {
    const { key, value } = req.body;
    if (!key || !value)
      return res.status(INVALID_DATA).send({ error: 'INVALID_DATA' });
    const data = await insert(key, value);
    res.status(SUCCESS).send(data);
  } catch (error) {
    throw error;
  }
};
