import ObjectModel from '../models/object.model';
import { Op } from 'sequelize';

/**
 *
 * @descripton
 * Functions that deal with the DB operations
 */
/**
 * @description find object by its key
 * @param {string} key
 * @param {number} timestamp
 * @returns {any}
 */
export const findOne = async (key, timestamp) => {
  try {
    const resultArray = await ObjectModel.findAll({
      limit: 1,
      where: {
        key,
        ...(timestamp && {
          timestamp: {
            [Op.lte]: timestamp,
          },
        }),
      },
      order: [['timestamp', 'DESC']],
      attributes: ['key', 'value', 'timestamp'],
    });
    if (resultArray?.length > 0) {
      const [result] = resultArray;
      const response = {
        key: result?.dataValues?.key,
        value: JSON.parse(result?.dataValues?.value),
        timestamp: result?.dataValues?.timestamp,
      };
      return response;
    }
    return null;
  } catch (error) {
    throw error;
  }
};
/**
 * @description Create or update the object if present
 * @param {string} key
 * @param {string} value
 * @returns {Promise}
 */
export const insert = async (key, value) => {
  try {
    const timestamp = Date.now();
    const result = await ObjectModel.create({
      key,
      value: JSON.stringify(value),
      timestamp,
    });
    if (result) {
      const response = {
        key: result?.dataValues?.key,
        value: JSON.parse(result?.dataValues?.value),
        timestamp: result?.dataValues?.timestamp,
      };
      return response;
    }
    return null;
  } catch (error) {
    throw error;
  }
};
