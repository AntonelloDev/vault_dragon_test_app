# Test app for Vault Dragon

## To run this locally (or locally with remote DB), add a .env file at the root of the project, with the following info, replacing the placeholders with the actual credentials.

- DATABASE = LOCAL_POSTGRES_DB_NAME
- DB_USER = LOCAL_POSTGRES_DB_USER
- DB_PASSWORD = LOCAL_POSTGRES_DB_PASSWORD
- PORT= PORT_NUMBER
- DB_URL= URL_REMOTE_DB
- DB_HOST = REMOTE_DB_HOST

- `npm install` to install the dependencies
- `npm run dev` to run in dev mode
- `npm start` to start the project
- `npm run test` to run the test suite
- IP address:PORT = `137.184.79.56:4000`
- See the docs in postman, by importing the docs-postman.json file in root dir
