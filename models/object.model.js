import { DataTypes } from 'sequelize';
import sequelize from '../db';
const ObjectModel = sequelize.define('object', {
  key: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: false,
  },
  value: {
    type: DataTypes.TEXT,
    allowNull: false,
    unique: false,
  },
  timestamp: {
    type: DataTypes.BIGINT,
  },
});
export default ObjectModel;
