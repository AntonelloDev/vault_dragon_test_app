import { NOT_FOUND } from './statusCodes';
/**
 *
 * @param {} _
 * @param {object} res
 * @returns {void}
 */
export const handleNotFound = (_, res) => {
  res.status(NOT_FOUND).send({
    message: 'NOT_FOUND',
  });
};
