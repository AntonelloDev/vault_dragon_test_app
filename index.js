import express from 'express';
const app = express();
const PORT = process.env.PORT;
const SERVER_HOST = '0.0.0.0';
import DB from './db';
import objectRoute from './api/object.route';
import methodOverride from 'method-override';
import { handleNotFound } from './util';
app.use(express.json());
app.use(methodOverride());
const rateLimit = require('express-rate-limit');
// Enable if behind a reverse proxy
app.set('trust proxy', 1);

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
});

//  apply to all requests
app.use(limiter);
app.use(function (_, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.header('Content-Type', 'application/json');
  next();
});
// REGISTER THE ROUTES
app.use('/api/object', objectRoute);
app.use('/*', handleNotFound);

// SYNC THE ORM AND START THE SERVER
(async () => {
  try {
    // await DB.drop();
    // console.log('Dropped database');
    await DB.sync({ alter: true });
    console.log('Connected to Database');
    // DON'T START THE SERVER IN TEST MODE
    if (process.env.NODE_ENV !== 'test') {
      app.listen(PORT, SERVER_HOST, () => {
        console.log(`Server listening on port ${PORT}`);
      });
    }
  } catch (error) {
    console.log('error', error);
    throw error;
  }
})();
export default app;
