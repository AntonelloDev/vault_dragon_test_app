import express from 'express';
const router = express.Router();
import { handleNotFound } from '../util';
import { findObject, insertObject } from '../controllers/object.controller';
// GET
router.get('/:key', findObject);
// POST
router.post('/', insertObject);
router.use('*', handleNotFound);
export default router;
