import request from 'supertest';
import app from '../index';

describe('****** Testing routes ******', () => {
  describe('GET /api/object', function () {
    it('responds with json and 404 status code if no match is found', (done) => {
      request(app)
        .get('/api/object/not_existing')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404, done);
    });
  });
  it('Should respond with 404 if a non existing route is requested', (done) => {
    request(app).get('/api/object/a/b/s/d').expect(404, done);
  });
  it('Should respond with 404 if a non existing route is requested', (done) => {
    request(app).get('/server/test/a/b/s/d').expect(404, done);
  });
  describe('POST /api/object', function () {
    const payload = { key: 'age', value: '39' };
    it('responds with json and 200 status code', (done) => {
      request(app)
        .post('/api/object')
        .send(payload)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('responds with json containing the sent object', (done) => {
      request(app)
        .post('/api/object')
        .send(payload)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body.key).toEqual(payload.key);
          expect(res.body.value).toEqual(payload.value);
          return done();
        });
    });
    it('Detects invalid data ', (done) => {
      request(app)
        .post('/api/object')
        .send({ key: undefined, value: null })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(422, done);
    });
    it('Detects missing data ', (done) => {
      request(app)
        .post('/api/object')
        .send()
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(422, done);
    });
  });
});
