import Sequelize from 'sequelize';
const { DB_HOST, DB_URL, DATABASE, DB_USER, DB_PASSWORD } = process.env;
const settings = {
  charset: 'utf8mb4',
  collate: 'utf8mb4_unicode_ci',
  dialect: 'postgres',
  host: DB_HOST,
  logging: false,
  createdAt: false,
  updatedAt: false,
};
let sequelize;
if (process.env.NODE_ENV !== 'test') {
  (settings.dialectOptions = {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  }),
    (sequelize = new Sequelize(DB_URL, settings));
} else {
  settings.host = 'localhost';
  sequelize = new Sequelize(DATABASE, DB_USER, DB_PASSWORD, settings);
}
export default sequelize;
